# API概述
DAEX Wallet为用户提供了三种API类型，管理API（mAPI）、交易API（eAPI）、交易验证API（ecAPI）。三种API分别处理管理、交易、交易确认指令。旨在帮助用户快速高效的完成交易。

# 相关文档

* [DAEX钱包API文档](http://www.daex.pro/doc/)
* [钱包API demo文档](https://gitlab.com/daex/daex-wallet-api-sdk/wikis/%E9%92%B1%E5%8C%85API-demo%E6%96%87%E6%A1%A3)